// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package bit

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

func reverse(s string) string {
	buf := make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		buf[i] = s[len(s)-i-1]
	}
	return string(buf)
}

func TestString(t *testing.T) {
	testCases := []uint64{
		0,
		1,
		0x8000000000000000,
		0x7FFFFFFFFFFFFFFF,
		0xFFFFFFFFFFFFFFFF,
		5577006791947779410,
	}
	for _, v := range testCases {
		a := NewArrayFromSlice([]uint64{v})
		want := reverse(fmt.Sprintf("%064b", v))
		if got := a.String(); got != want {
			t.Errorf("want %s, got %s", want, got)
		}
	}
}

func TestLen(t *testing.T) {
	a := NewArray(33)
	if a.Len() != 33 {
		t.Errorf("want %d, got %d", 33, a.Len())
	}
}

func TestAdd(t *testing.T) {
	a := Array{}
	a.Add(1)
	want := "1"
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
	a.Add(0)
	want += "0"
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
	for i := 0; i < 62; i++ {
		a.Add(1)
	}
	want += strings.Repeat("1", 62)
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
	a.Add(0)
	want += "0"
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
}

func TestAddBits(t *testing.T) {
	a := Array{}
	a.AddBits(7, 4)
	want := "1110"
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
	a.AddBits(5, 7)
	want += "1010000"
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
	a.AddBits(0xFFFFFFFFFFFFFFFF, 55)
	want += strings.Repeat("1", 55)
	if got := a.String(); got != want {
		t.Errorf("want %s, got %s", want, got)
	}
}

func TestGet(t *testing.T) {
	a := NewArrayFromSlice([]uint64{1, 0x8000000000000000})
	if got := a.Get(0); got != 1 {
		t.Errorf("want 1, got %d", got)
	}
	if got := a.Get(1); got != 0 {
		t.Errorf("want 0, got %d", got)
	}
	if got := a.Get(63); got != 0 {
		t.Errorf("want 0, got %d", got)
	}
	if got := a.Get(64); got != 0 {
		t.Errorf("want 0, got %d", got)
	}
	if got := a.Get(126); got != 0 {
		t.Errorf("want 0, got %d", got)
	}
	if got := a.Get(127); got != 1 {
		t.Errorf("want 1, got %d", got)
	}
}

func TestGetBool(t *testing.T) {
	a := NewArrayFromSlice([]uint64{1})
	if got := a.GetBool(0); !got {
		t.Errorf("want true, got false")
	}
	if got := a.GetBool(1); got {
		t.Errorf("want false, got true")
	}
}

func TestSet(t *testing.T) {
	a := NewArray(128)
	a.Set(0)
	a.Set(127)
	if bits := a.Bits(); len(bits) != 2 || bits[0] != 1 || bits[1] != 0x8000000000000000 {
		t.Errorf("want %v, got %v", []uint{1, 0x8000000000000000}, bits)
	}
}

func TestClear(t *testing.T) {
	a := NewArrayFromSlice([]uint64{1, 0x8000000000000000})
	a.Clear(0)
	a.Clear(127)
	if bits := a.Bits(); len(bits) != 2 || bits[0] != 0 || bits[1] != 0 {
		t.Errorf("want %v, got %v", []uint{0, 0}, bits)
	}
}

func TestSetBit(t *testing.T) {
	a := NewArrayFromSlice([]uint64{3, 0})
	a.SetBit(1, 0)
	a.SetBit(127, 1)
	if bits := a.Bits(); len(bits) != 2 || bits[0] != 1 || bits[1] != 0x8000000000000000 {
		t.Errorf("want %v, got %v", []uint{1, 0x8000000000000000}, bits)
	}
}

func TestSetBitBool(t *testing.T) {
	a := NewArrayFromSlice([]uint64{3, 0})
	a.SetBitBool(1, false)
	a.SetBitBool(127, true)
	if bits := a.Bits(); len(bits) != 2 || bits[0] != 1 || bits[1] != 0x8000000000000000 {
		t.Errorf("want %v, got %v", []uint{1, 0x8000000000000000}, bits)
	}
}

func TestToggle(t *testing.T) {
	a := NewArrayFromSlice([]uint64{3, 0})
	a.Toggle(1)
	a.Toggle(127)
	if bits := a.Bits(); len(bits) != 2 || bits[0] != 1 || bits[1] != 0x8000000000000000 {
		t.Errorf("want %v, got %v", []uint{1, 0x8000000000000000}, bits)
	}
}

func TestWriteTo(t *testing.T) {
	var buf bytes.Buffer
	a := &Array{}
	if n, err := a.WriteTo(&buf); n != 0 || err != nil {
		t.Fatalf("want 0,nil; got %d,%v", n, err)
	}
	if buf.Len() > 0 {
		t.Fatalf("buf.Len() must be 0")
	}
	a = NewArrayFromSlice([]uint64{0, 1, 2, 3})
	if n, err := a.WriteTo(&buf); n != 32 || err != nil {
		t.Fatalf("want 32,nil; got %d,%v", n, err)
	}
	want := []byte{
		0, 0, 0, 0, 0, 0, 0, 0,
		1, 0, 0, 0, 0, 0, 0, 0,
		2, 0, 0, 0, 0, 0, 0, 0,
		3, 0, 0, 0, 0, 0, 0, 0,
	}
	if !bytes.Equal(want, buf.Bytes()) {
		t.Fatalf("want %v, got %v", want, buf.Bytes())
	}
}

func TestUint64(t *testing.T) {
	want := uint64(5577006791947779410)
	a := NewArrayFromSlice([]uint64{want})
	if got := a.Uint64(); got != want {
		t.Errorf("want %d, got %d", want, got)
	}
}

func TestPopcnt(t *testing.T) {
	a := NewArrayFromSlice([]uint64{5577006791947779410, 1})
	const want = 32
	if got := a.Popcnt(); got != want {
		t.Errorf("want %d, got %d", want, got)
	}
}

func TestCopy(t *testing.T) {
	a1 := NewArrayFromSlice([]uint64{5577006791947779410, 1})
	a2 := a1.Copy()
	if a1.String() != a2.String() {
		t.Errorf("Copy failed")
	}
}

func TestReset(t *testing.T) {
	a := NewArrayFromSlice([]uint64{1})
	a.Reset()
	if a.Len() != 0 || len(a.Bits()) > 0 {
		t.Errorf("Reset failed")
	}
}
