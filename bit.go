// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package bit provides a Go implementation of bit arrays.
package bit

import (
	"encoding/binary"
	"io"
)

// An Array is a variable-sized array of bits.
// The zero value for Array is an empty Array ready to use.
type Array struct {
	length int
	bits   []uint64
}

func divRoundUp(a, b int) int {
	c := a / b
	if a%b > 0 {
		c++
	}
	return c
}

// NewArray returns a new bit array of n zero bits.
func NewArray(n int) *Array {
	return &Array{
		length: n,
		bits:   make([]uint64, divRoundUp(n, 64)),
	}
}

// NewArrayFromSlice constructs and returns a new bit array from a slice of uint64s.
func NewArrayFromSlice(bits []uint64) *Array {
	return &Array{
		length: len(bits) * 64,
		bits:   bits,
	}
}

// Len returns the number of bits in the array.
func (a *Array) Len() int {
	return a.length
}

// Bits returns raw bits stored in the array as a slice of uint64s.
func (a *Array) Bits() []uint64 {
	return a.bits
}

// String returns the contents of the array as a string of 0s and 1s.
func (a *Array) String() string {
	buf := make([]byte, a.length)
	i := 0 // current index into buf
	for j, n := 0, a.length/64; j < n; j++ {
		v := a.bits[j]
		r := 64
		for r > 0 {
			buf[i] = '0' + byte(v&1)
			v >>= 1
			r--
			i++
		}
	}
	if r := a.length % 64; r > 0 {
		v := a.bits[len(a.bits)-1]
		for r > 0 {
			buf[i] = '0' + byte(v&1)
			v >>= 1
			r--
			i++
		}
	}
	return string(buf)
}

// Add adds a bit to the array.
func (a *Array) Add(v int) {
	if v&^1 != 0 {
		panic("argument must be either 0 or 1")
	}
	if shift := uint(a.length) % 64; shift > 0 {
		i := len(a.bits) - 1
		a.bits[i] |= uint64(v) << shift
	} else {
		a.bits = append(a.bits, uint64(v))
	}
	a.length++
}

// AddBits adds the n least significant bits of v to the array.
func (a *Array) AddBits(v uint64, n int) {
	if n < 0 || n > 64 {
		panic("n must be in range [0,64]")
	}
	if n == 0 {
		return
	}
	v &= uint64(1<<uint(n) - 1)
	if r := a.length % 64; r == 0 {
		a.bits = append(a.bits, v)
	} else {
		avail := 64 - r
		if n <= avail {
			a.bits[len(a.bits)-1] |= v << uint(r)
		} else {
			mask := uint64(1)<<uint(avail) - 1
			a.bits[len(a.bits)-1] |= (v & mask) << uint(r)
			a.bits = append(a.bits, v>>uint(avail))
		}
	}
	a.length += n
}

// Get returns the value of the nth bit.
func (a *Array) Get(n int) int {
	i := n / 64
	shift := uint(n % 64)
	return int((a.bits[i] >> shift) & 1)
}

// GetBool returns the value of the nth bit as true (for 1) or false (for 0).
func (a *Array) GetBool(n int) bool {
	return a.Get(n) == 1
}

// Set sets the nth bit to 1.
func (a *Array) Set(n int) {
	i := n / 64
	shift := uint64(n % 64)
	a.bits[i] |= 1 << shift
}

// Clear sets the nth bit to 0.
func (a *Array) Clear(n int) {
	i := n / 64
	shift := uint64(n % 64)
	a.bits[i] &= ^(1 << shift)
}

// SetBit sets the nth bit to the value of the least significant bit of v,
// which must be either 0 or 1.
func (a *Array) SetBit(n, v int) {
	if v&^1 != 0 {
		panic("second argument must be either 0 or 1")
	}
	i := n / 64
	shift := uint64(n % 64)
	if v == 1 {
		a.bits[i] |= 1 << shift
	} else {
		a.bits[i] &= ^(1 << shift)
	}
}

// SetBitBool sets the nth bit to 0 if b is false and to 1 otherwise.
func (a *Array) SetBitBool(n int, b bool) {
	i := n / 64
	shift := uint64(n % 64)
	if b {
		a.bits[i] |= 1 << shift
	} else {
		a.bits[i] &= ^(1 << shift)
	}
}

// Toggle toggles the nth bit, setting it to 0 if it was 1 and vice versa.
func (a *Array) Toggle(n int) {
	i := n / 64
	shift := uint64(n % 64)
	a.bits[i] ^= 1 << shift
}

// Uint64 returns the bits stored in the array as a uint64.
// It panics if the array is empty or too big.
func (a *Array) Uint64() uint64 {
	if len(a.bits) == 0 {
		panic("array is empty")
	}
	if len(a.bits) > 1 {
		panic("array does not fit in a uint64")
	}
	return a.bits[0]
}

// Popcnt returns the number of set bits in the array.
func (a *Array) Popcnt() int {
	const (
		m1  = 0x5555555555555555
		m2  = 0x3333333333333333
		h01 = 0x0101010101010101
		m4  = 0x0f0f0f0f0f0f0f0f
	)
	popcnt := 0
	for _, x := range a.bits {
		x -= (x >> 1) & m1
		x = (x & m2) + ((x >> 2) & m2)
		x = (x + (x >> 4)) & m4
		popcnt += int((x * h01) >> 56)
	}
	return popcnt
}

// Copy returns a deep copy of the array.
func (a *Array) Copy() *Array {
	bits := make([]uint64, len(a.bits))
	copy(bits, a.bits)
	return &Array{
		length: a.length,
		bits:   bits,
	}
}

// Reset empties the array.
func (a *Array) Reset() {
	a.bits = a.bits[:0]
	a.length = 0
}

// WriteTo writes the contents of the array to w.
// It implements the io.WriterTo interface.
func (a *Array) WriteTo(w io.Writer) (n int64, err error) {
	if a.length == 0 {
		return 0, nil
	}

	buf := make([]byte, 8)
	for _, v := range a.bits {
		binary.LittleEndian.PutUint64(buf, v)
		var m int
		m, err = w.Write(buf)
		n += int64(m)
		if err != nil {
			return
		}
		// all bytes should have been written, by definition of
		// Write method in io.Writer
		if m != 8 {
			return n, io.ErrShortWrite
		}
	}
	return
}
