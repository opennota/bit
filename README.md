bit [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/bit?status.svg)](http://godoc.org/gitlab.com/opennota/bit) [![Pipeline status](https://gitlab.com/opennota/bit/badges/master/pipeline.svg)](https://gitlab.com/opennota/bit/commits/master)
===

Package bit provides a Go implementation of bit arrays.

## Install

    go get -u gitlab.com/opennota/bit

## Similar repositories

- https://github.com/robskie/bit
